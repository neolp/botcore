

module.exports = function create(source, onUpdate) {
    let prox = new Proxy(source, {
      get(target, prop) {
        if (target[prop] instanceof Node) {
          return target[prop].state
        }
        return target[prop]
      },
      set(target, prop, value) {
        let oldvalue = target[prop]
        target[prop] = value
        onUpdate(prop, { [prop]: value }, { [prop]: oldvalue } )
        return true
      }
    })
    vm.createContext(prox) // Contextify the sandbox.

    return prox
  }
