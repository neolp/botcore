//const cbor = require('cbor')
const cbor = require('cbor-sync')

const leveldown = require('leveldown')

class tsdb {
  constructor(dbname) {
    this.tsdb = leveldown(`${dbname}.tsdb`)
    this.tsdb.open((err) => {
      if (err) {
        console.error('tsdb')
      }
    })

    process.on('SIGTERM', () => {
      this.tsdb.close()
      console.error('flush tsdb on shutdown')
      process.exitCode = 1 // ; .exit(1)
      setTimeout(() => { process.exit(1) }, 100)
    })
    process.on('SIGINT', () => {
      this.tsdb.close(() => {
        console.error('flush tsdb on shutdown')
        process.exitCode = 1 // ; .exit(1)
        setTimeout(() => { process.exit(1) }, 100)
      })
    })
  }
  write(seriesname, value, timestamp) {
    if (!timestamp) timestamp = Date.now()
    let key = Buffer.from(`${seriesname}.000000`)
    key.writeUIntBE(timestamp, key.length - 6, 6)
    this.tsdb.put(key, cbor.encode(value), function (err) {
      if (err) {
        return console.log('Ooops!', err) // some
      }
    })
  }

  _getfirst(param) {
    let prom = new Promise((resolve, reject) => {
      let ret = []
      let varname = ''
      let iterator = this.tsdb.iterator(param)
      let append = (err, key, value) => {
        if (err) {
          iterator.end(() => { reject(new Error(err)) })
        } else if (key) {
          let timestamp = key.readUIntBE(key.length - 6, 6)
          varname = key.toString('utf8', 0, key.length - 6 - 1)
          let v = cbor.decode(value)
          ret.push([timestamp, v])
          iterator.next(append)
        } else {
          iterator.end(() => { resolve({ varname, ret }) })
        }
      }
      iterator.next(append)


      /*
            this.db.createReadStream(param)
              .on('data', function (data) {
                // console.log(data.key, '=', data.value)
                // let timestamp = new Date(data.key.readUIntBE(data.key.length - 6, 6))
                let timestamp = data.key.readUIntBE(data.key.length - 6, 6)
                varname = data.key.toString('utf8', 0, data.key.length - 6 - 1)
                let value = cbor.decode(data.value)
                // ret.push({timestamp: timestamp, value: value})
                ret.push([timestamp, value])
                // console.log(timestamp, '=', value)
              })
              .on('error', function (err) {
                console.log('Oh my!', err)
                reject(new Error(err))
              })
              .on('close', function () {
                // console.log('Stream closed')
              })
              .on('end', function () {
                // console.log('Stream ended')
                resolve({varname,ret})
              })
              */
    })
    return prom
  }
  async gethistories() {
    let ret = []
    let key = Buffer.from('')
    while (true) {
      let param = { gt: key, limit: 1 }
      try {
        let first = await this._getfirst(param)
        if (!first || !first.varname)
          break
        key = Buffer.from(`${first.varname}.000000`)
        key.writeUIntBE(0xFFFFFFFFFFFF, key.length - 6, 6) // 6 bytes of FF
        let last = await this._getfirst({ lt: key, limit: 1, reverse: true })
        ret.push({ name: first.varname, first: first.ret[0], last: last.ret[0]/*,firstd:new Date(first.ret[0][0]).toDateString()*/ })
      } catch (er) {
        break
      }
    }
    return ret
  }
  select(seriesname, from, to, limit) {
    let keyfrom
    if (!from) {
      keyfrom = Buffer.from(`${seriesname}.`)
    } else {
      keyfrom = Buffer.from(`${seriesname}.000000`)
      keyfrom.writeUIntBE(from, keyfrom.length - 6, 6)
    }
    let keyto
    if (!to) {
      to = 0xFFFFFFFFFFFF // 6 bytes of FF
    }
    keyto = Buffer.from(`${seriesname}.000000`)
    keyto.writeUIntBE(to, keyto.length - 6, 6)
    let prom = new Promise((resolve, reject) => {
      let ret = []
      let param = { gte: keyfrom, lte: keyto }
      param.reverse = true
      if (limit) {
        param.limit = limit
      }
      let iterator = this.tsdb.iterator(param)
      let append = (err, key, value) => {
        if (err) {
          iterator.end(() => { reject(new Error(err)) })
        } else if (key) {
          let timestamp = key.readUIntBE(key.length - 6, 6)
          //varname = key.toString('utf8', 0, key.length - 6 - 1)
          let v = cbor.decode(value)
          // ret.push({timestamp: timestamp, value: value})
          ret.push([timestamp, v])
          iterator.next(append)
        } else {
          iterator.end(() => { resolve(ret) })
        }
      }
      iterator.next(append)
      /*
      this.db.createReadStream(param)
        .on('data', function (data) {
          // console.log(data.key, '=', data.value)
          // let timestamp = new Date(data.key.readUIntBE(data.key.length - 6, 6))
          let timestamp = data.key.readUIntBE(data.key.length - 6, 6)
          let value = cbor.decode(data.value)
          // ret.push({timestamp: timestamp, value: value})
          ret.push([timestamp, value])
          // console.log(timestamp, '=', value)
        })
        .on('error', function (err) {
          console.log('Oh my!', err)
          reject(new Error(err))
        })
        .on('close', function () {
          // console.log('Stream closed')
        })
        .on('end', function () {
          // console.log('Stream ended')
          resolve(ret)
        })
        */
    })
    return prom

    /*
    return this.db.createReadStream({gte: keyfrom, lte: keyto})
      .pipe(through(function (data) {
        // var parts = data.key.toString().split('.')

        data.key = seriesname
        data.value = cbor.decode(data.value)

        this.queue(data)
      }))
*/
    /*
    if (!Array.isArray(seriesnames)) {
      seriesnames = [seriesnames]
    }
    for (let i = 0; i < seriesnames.length; i++) {
      let Ts = this.series.get(seriesnames[i])
      if (Ts === undefined) {
        Ts = new Ts(seriesnames[i])
        this.series.set(seriesnames[i], Ts)
      }
      if (from <= Ts.lasttimestamp) {
        throw (new Error('wrong timestamp'))
      }
    }

    let cursor = null
    return cursor
    */
  }
  // cursorread (cursor, elementsnumber) { }

  flush() {
    /*
    this.series.forEach((ts, key, map) => {
      // let e=new cbor.Encoder();
      // let f=fs.createWriteStream(`tsdb/${this.dbname}_${key}.db`,{flags:"a"});
      // let tf = fs.createWriteStream(`tsdb/${this.dbname}_${key}.txt`, { flags: "a" });
      let encb = []
      // e.pipe(f);
      // let enc = new cbor.Encoder();
      for (let i = ts.notwritten; i < ts.timestamps.length; i++) {
        encb.push(cbor.encode(ts.timestamps[i]))
        encb.push(cbor.encode(ts.values[i]))

        //        e.write(ts.timestamps[i]);
        //        e.write(ts.values[i]);
        // tf.write(`${ts.timestamps[i]},${ts.values[i]}\r\n`);
      }
      ts.notwritten = ts.timestamps.length
      let totlen = 0
      for (let i = 0; i < encb.length; i++) {
        totlen += encb[i].length
      }
      let wbuf = Buffer.alloc(totlen)
      let offset = 0
      for (let i = 0; i < encb.length; i++) {
        encb[i].copy(wbuf, offset)
        offset += encb[i].length
      }
      fs.appendFileSync(`tsdb/${this.dbname}_${key}.db`, wbuf)
      // f.close();
      // tf.close();
    })
    */
  }
}

module.exports = tsdb
