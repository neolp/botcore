const platform = require('./cpudetect')()


module.exports = {
  core: require('./core'),
  Node: require('./botnode'),
  Led: require('./led'),
  Pin: require('./pin'),
}

//module.exports.Boards = module.exports.Board.Collection