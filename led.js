const core = require('./core')
const Node = require('./botnode')

class Led extends Node {

  constructor(name, cfg) {
    super(cfg)
    if (typeof cfg === 'string') cfg = { addr: cfg };
    ({ connector: this.connector, fullhost: this.addr } = core.getConnector(cfg.addr))
    core.addNode(name, this)

    this.connector.call(this.addr, 'config', ['OUTPUT', 'LOW'])
    this.set('value', cfg.value ? 1 : 0)
    this.subscribe('value', (name, value) => {
      this.connector.call(this.addr, 'set', [value]).then(() => { }).catch(() => { })
    })
    this.populate(['on', 'off', 'blink'])
  }

  on() {
    this.set('value', 1)
  }
  off() {
    this.set('value', 0)
  }
  blink() {
    this.set('value', 0)
  }
}

module.exports = Led