var fs = require('fs');
var PI_MODELS = {
  'BCM2708': { isPi: true, isWin: false },
  'BCM2709': { isPi: true, isWin: false },
  'BCM2710': { isPi: true, isWin: false },
  'BCM2711': { isPi: true, isWin: false },
  'BCM2835': { isPi: true, isWin: false },
  'BCM2837B0': { isPi: true, isWin: false }
};

const rpicodes = {
  '900021': { model: 'A+', revision: '1.1', ram: '512MB', manufacturer: 'Sony UK' },
  '900032': { model: 'B+', revision: '1.2', ram: '512MB', manufacturer: 'Sony UK' },
  '900092': { model: 'Zero', revision: '1.2', ram: '512MB', manufacturer: 'Sony UK' },
  '900093': { model: 'Zero', revision: '1.3', ram: '512MB', manufacturer: 'Sony UK' },
  '9000c1': { model: 'Zero W', revision: '1.1', ram: '512MB', manufacturer: 'Sony UK' },
  '9020e0': { model: '3A+', revision: '1.0', ram: '512MB', manufacturer: 'Sony UK' },
  '920092': { model: 'Zero', revision: '1.2', ram: '512MB', manufacturer: 'Embest' },
  '920093': { model: 'Zero', revision: '1.3', ram: '512MB', manufacturer: 'Embest' },
  '900061': { model: 'CM', revision: '1.1', ram: '512MB', manufacturer: 'Sony UK' },
  'a01040': { model: '2B', revision: '1.0', ram: '1GB', manufacturer: 'Sony UK' },
  'a01041': { model: '2B', revision: '1.1', ram: '1GB', manufacturer: 'Sony UK' },
  'a02082': { model: '3B', revision: '1.2', ram: '1GB', manufacturer: 'Sony UK' },
  'a020a0': { model: 'CM3', revision: '1.0', ram: '1GB', manufacturer: 'Sony UK' },
  'a020d3': { model: '3B+', revision: '1.3', ram: '1GB', manufacturer: 'Sony UK' },
  'a02042': { model: '2B(with BCM2837)', revision: '1.2', ram: '1GB', manufacturer: 'Sony UK' },
  'a21041': { model: '2B', revision: '1.1', ram: '1GB', manufacturer: 'Embest' },
  'a22042': { model: '2B(with BCM2837)', revision: '1.2', ram: '1GB', manufacturer: 'Embest' },
  'a22082': { model: '3B', revision: '1.2', ram: '1GB', manufacturer: 'Embest' },
  'a220a0': { model: 'CM3', revision: '1.0', ram: '1GB', manufacturer: 'Embest' },
  'a32082': { model: '3B', revision: '1.2', ram: '1GB', manufacturer: 'Sony Japan' },
  'a52082': { model: '3B', revision: '1.2', ram: '1GB', manufacturer: 'Stadium' },
  'a22083': { model: '3B', revision: '1.3', ram: '1GB', manufacturer: 'Embest' },
  'a02100': { model: 'CM3+', revision: '1.0', ram: '1GB', manufacturer: 'Sony UK' },
  'a03111': { model: '4B', revision: '1.1', ram: '1GB', manufacturer: 'Sony UK' },
  'b03111': { model: '4B', revision: '1.1', ram: '2GB', manufacturer: 'Sony UK' },
  'c03111': { model: '4B', revision: '1.1', ram: '4GB', manufacturer: 'Sony UK' },
}
module.exports = function () {
  let ret
  switch (process.platform) {
    case 'win32':
      return {
        isPi: false,
        isWin: true
      }
      break
    case 'linux':
      ret = { isPi: false, isWin: false }
      var cpuInfo
      try {
        cpuInfo = fs.readFileSync('/proc/cpuinfo', { encoding: 'utf8' })
      } catch (e) {
        return { isPi: false, isWin: false }
      }
      cpuInfo
        .split('\n')
        .map(line => line.replace(/\t/g, ''))
        .filter(line => line.length > 0)
        .map(line => line.split(':'))
        .map(pair => pair.map(entry => entry.trim()))
        .filter(pair => pair[0] === 'Hardware' | pair[0] === 'Revision' | pair[0] === 'Serial')
        .forEach((pair => { ret[pair[0]] = pair[1] }))

      if (PI_MODELS[ret.Hardware]) return Object.assign(ret, PI_MODELS[ret.Hardware], rpicodes[ret.Revision])
      return ret
      break
  }
  return { isPi: false, isWin: false }
}
