/* eslint-disable no-console */
const fs = require('fs')
const Node = require('./botnode')
const cbor = require('cbor-sync')
const UIDGenerator = require('uid-generator')
const uidgen = new UIDGenerator()
const Leveldown = require('leveldown')

const itmpsubscriptions = new Map()
const tsdb = require('./tsdb')

// class Queue {
//   constructor() {
//     this._oldestIndex = 1
//     this._newestIndex = 1
//     this._storage = {}
//   }

//   size() {
//     return this._newestIndex - this._oldestIndex
//   }

//   enqueue(data) {
//     this._storage[this._newestIndex] = data
//     this._newestIndex++
//   }

//   dequeue() {
//     var oldestIndex = this._oldestIndex,
//       newestIndex = this._newestIndex,
//       deletedData

//     if (oldestIndex !== newestIndex) {
//       deletedData = this._storage[oldestIndex]
//       delete this._storage[oldestIndex]
//       this._oldestIndex++

//       return deletedData
//     }
//   }
// }

// class HistoryStorage {
//   constructor() {
//     this.history = new Map()
//   }

//   async appendHistory(uri, value, time) {
//     let h
//     if (this.history.has(uri)) {
//       h = this.history.get(uri)
//     } else {
//       h = []
//       this.history.set(uri, h)
//     }
//     if (!time) time = Date.now()
//     h.push([value, time])
//   }
//   async getHistory(uri, from, to, limit) {
//     if (!this.history.has(uri)) {
//       return undefined
//     }
//     let h = this.history.get(uri)
//     let start = h.length - 1
//     if (to) {
//       start = start - 1
//     }
//     let finish = start - (limit ? limit : h.length)
//     if (finish < 0) finish = 0

//     let res = []
//     for (let i = start; i >= finish; i--) {
//       let val = h[i]
//       if (from && val[1] < from)
//         break
//       res.push(h[i])
//     }
//     return res
//   }

// }

const uriregexp = /^(?<schema>[a-z]+)(?:.(?<subschema>[a-z]+))?:(?:\/\/)?(?:(?:(?<login>(?:[a-z0-9-._]|%[0-9A-F]{2})*)(?::(?<pass>(?:[a-z0-9-_]|%[0-9A-F]{2})*))?@)?(?<fullhost>(?<host>(?:[a-z0-9-_~.]|%[0-9A-F]{2})*|\[[0-9a-fA-F:.]{2,}\])(?::(?<port>[0-9]+))?))(?<path>(?:\/?(?:[a-zA-Z0-9-._~!$&'()*+,;=:@]|%[0-9A-F]{2})*)*)(?<query>\?(?:[A-Za-z0-9\-._~!$&'()*+,;=:@/?]|%[0-9A-Fa-f]{2})*)?(?<fragment>#(?:[A-Za-z0-9\-._~!$&'()*+,;=:@/?]|%[0-9A-Fa-f]{2})*)?$/i
class Core {
  constructor() {
    this.connectors = new Map()
    this.node = new Node(undefined, this)
    this.initialized = false
  }

  init(cfg, localModulesDir) {
    this.localModulesDir = localModulesDir
    for (const linkName in cfg.listen) {
      this.addServer(cfg.listen[linkName].url, cfg.listen[linkName], localModulesDir)
    }

    for (const nodeName in cfg.nodes) {
      try {
        //let NodeClass = require(cfg.nodes[nodeName].module)
        let NodeClass
        try {
          NodeClass = require(cfg.nodes[nodeName].module)
        } catch (e) {
          NodeClass = require(`${localModulesDir}/${cfg.nodes[nodeName].module}`)
        }
        let nd = new NodeClass(cfg.nodes[nodeName].cfg)
        this.node.addNode(nodeName, nd)
      } catch (e) {
        console.error(e)
      }
    }
    //this.history = new HistoryStorage()
  }

  addServer(linkName, cfg, localModulesDir) {
    let Link
    if (cfg.module) {
      try {
        Link = require(cfg.module)
      } catch (e) {
        if (localModulesDir)
          Link = require(`${localModulesDir}/${cfg.module}`)
        else
          Link = require(`./${cfg.module}`)
      }
    }
    try {
      const server = Link.createServer(linkName, cfg, (newclient) => {
        this.addReadyConnector(newclient)
      })
    } catch (e) {

    }
  }
  addConnector(url, cfg, localModulesDir) {
    let Link
    if (cfg.module) {
      try {
        Link = require(cfg.module)
      } catch (e) {
        if (localModulesDir)
          Link = require(`${localModulesDir}/${cfg.module}`)
        else
          Link = require(`./${cfg.module}`)
      }
    }
    try {
      let lnk = Link.connect(url, cfg)
      this.addReadyConnector(lnk)
      this.connectors.set(url, lnk)
      return lnk
    } catch (e) {
      console.error(e)
    }
  }

  addReadyConnector(lnk) {
    lnk.on(lnk.$subscribe, (topic, opts) => {
      //console.log(`subscribe ${topic}@${addr}`)
      console.log('subscribe', lnk.url, 'for', topic)
      if (itmpsubscriptions.has(`${topic}@${lnk.url}`)) {
        console.error('panic! double subs for ', topic, lnk.url)
      }
      let subsid
      // if (opts) {
      //   //if (typeof opts === 'string') {         }
      //   //let queue = new Queue()
      //   let onair = 0
      //   let onaircnt = 0
      //   subsid = this.subscribe(topic, (loctopic, value) => {
      //     if (onair < 2) {
      //       onair++

      //       let start = Date.now()
      //       lnk.publish(topic, value).then(() => {
      //         onair--
      //         onaircnt++
      //         if (onaircnt % 10 == 0)
      //           console.log(`+ ${Date.now() - start}`)
      //       }).catch(() => {
      //         onair--
      //         //onaircnt++
      //         //                  if (onaircnt % 10 == 0)
      //         console.log(`- ${Date.now() - start}`)
      //       })
      //       // lnk.sendEvent(topic, value).then(() => {
      //       //   onair--
      //       // })
      //     } else {
      //       console.log('skip frame')
      //     }
      //   })
      //} else {
      if (topic.endsWith('[]')) { // ask for history data and history value (format of transferred data [timestamp, value])
        subsid = this.subscribe(topic.substring(0, topic.length - 2), (loctopic, value) => { // ask for 'normal' topic
          lnk.sendEvent(topic, [Date.now(), value]).catch(() => { })
        })
      } else if (opts && typeof opts === 'object') {
        subsid = this.subscribe(topic, (loctopic, value) => {
          lnk.sendEvent(topic, value, { t: Date.now() }).catch(() => { })
        })
      } else {
        subsid = this.subscribe(topic, (loctopic, value) => {
          //lnk.publish(topic, value)
          lnk.sendEvent(topic, value).catch(() => { })
        })
      }
      //}
      if (!subsid) {
        console.error('subscribe error', topic)
        throw (new Error('subscribe error'))
      } else {
        itmpsubscriptions.set(`${topic}@${lnk.url}`, subsid)

        if (topic.endsWith('[]')) { // send initial data
          if (typeof opts !== 'object') opts = {}
          this.getHistory(topic.substring(0, topic.length - 2), opts.from, opts.to, opts.limit).then((vs) => {
            if (Array.isArray(vs))
              vs.forEach((v) => {
                lnk.sendEvent(topic, v)
              })
          })
        } else if (opts && typeof opts === 'object') {
          this.getHistory(topic, opts.from, opts.to, opts.limit).then((vs) => {
            if (Array.isArray(vs))
              vs.forEach((v) => {
                lnk.sendEvent(topic, v[1], { t: v[0] })
              })
          })
        } else {
          let v = this.getValue(topic)
          if (v !== undefined && v !== null && typeof v !== 'function')
            setImmediate(() => {
              lnk.sendEvent(topic, v)
              console.log('sent', ' ', topic, ':', v)
            })
        }
      }
    })
    lnk.on(lnk.$unsubscribe, (topic) => {
      //console.log(`${topic}@${addr}`)
      console.log('unsubscribe', lnk.url, 'for', topic)
      this.unsubscribe(topic, itmpsubscriptions.get(`${topic}@${lnk.url}`))
      itmpsubscriptions.delete(`${topic}@${lnk.url}`)
    })
    if (lnk.setGeneralCall)
      lnk.setGeneralCall((uri, args, opts) => {
        //console.log(call `${topic}@${addr}`)
        //console.log('call "' + uri + '"(', args, ')')
        let result = this.call(uri, args, opts)
        //console.log('call return "' + result + '"')
        return result
      })
    lnk.on(lnk.$message, (link, url, val) => {
      //          console.log(link, from, url, val)
      console.log('event', url, JSON.stringify(val))
      this.updateValue(url, val)
    })

    return lnk
  }



  getConnector(url) {
    let parse = url.match(uriregexp)
    if (!parse || !parse.groups)
      return undefined
    let connector = this.connectors.get(url)
    if (!connector) {
      connector = this.addConnector(url, { module: parse.groups.schema, url })
    }
    return connector

    // itmp://com:1
    // let delim = name.indexOf(':')
    // if (delim < 0) return [this.connectors.get(name), undefined]
    // return [this.connectors.get(name.substring(0, delim)), name.substring(delim + 1)]
    //    let parts = name.split(':', 2)
    //    return [this.connectors.get(parts[0]), parts[1]]
  }

  addNode(uri, newnode) {
    if (typeof uri !== 'string') return undefined
    let path = uri.split('/')
    if (path[0] == '') path.shift()

    if (path.length < 1) { return undefined }
    let node = this.node
    for (let i = 0; i < path.length - 1; i++) {
      if (node && node.getChild) {
        node = node.getChild(path[i])
      } else {
        let tmpnode = new Node({}, core)
        node.addNode(path[i], tmpnode)
        node = tmpnode
      }
    }
    node.addNode(path[path.length - 1], newnode)
    //return node
  }

  getNode(uri) {
    if (typeof uri !== 'string') return undefined
    let path = uri.split('/')
    if (path[0] == '') path.shift()

    if (path.length < 1) { return undefined }
    let node = this.node
    for (let i = 0; i < path.length; i++) {
      if (node && node.getChild) {
        node = node.getChild(path[i])
      } else {
        return undefined
      }
    }
    return node
  }

  deleteNode(uri, newnode) {
    if (typeof uri !== 'string') return undefined
    let path = uri.split('/')
    if (path[0] == '') path.shift()

    if (path.length < 1) { return undefined }
    let node = this.node
    for (let i = 0; i < path.length - 1; i++) {
      if (node && node.getChild) {
        node = node.getChild(path[i])
      } else {
        return undefined
      }
    }
    node.deleteNode(path[path.length - 1])
    //return node
  }

  persistValue(fulname, value, force) {
    if (!this.initialized && !force) {
      return
    }
    if (!this.db) {
      console.warn('no persistent db connected to store', fulname, value)
      return
    }
    this.db.put(fulname, cbor.encode(value), (err) => {
      if (err) {
        console.error('write error', fulname, value, err)
      } else {
        console.log('write ok', fulname, value)
      }
    })
  }
  persistChild(fulname, nodeName, node) {
    if (!this.initialized) {
      return
    }
    if (!this.db) {
      console.warn('no persistent db connected to store node', fulname, nodeName)
      return
    }
    this.db.put(fulname + '#' + nodeName, cbor.encode({ name: nodeName, module: node.module || node.constructor.name, childrenPersistent: node.childrenPersistent }), (err) => {
      if (err) {
        console.error('write node error', fulname, nodeName, err)
      } else {
        console.log('write node ok', fulname, nodeName)
      }
    })
  }
  persistChildDelete(fulname, nodeName) {
    if (!this.db) {
      console.warn('no persistent db connected to delete node', fulname, nodeName)
      return
    }
    this.db.del(fulname + '#' + nodeName, (err) => {
      if (err) {
        console.error('delete node error', fulname, nodeName, err)
      } else {
        console.log('delete node ok', fulname, nodeName)
      }
    })
  }
  async deleteNodeValues(fullname) {
    let keyfrom = fullname + '.'
    let keyto = fullname + '/' // '/' is just after '.'
    let prom = new Promise((resolve, reject) => {
      let ret = []
      let param = { gte: keyfrom, lt: keyto }   //param.reverse = true
      let iterator = this.db.iterator(param)
      let append = (err, key, value) => {
        if (err) {
          iterator.end(() => { reject(new Error(err)) })
        } else if (key) {
          ret.push(key.toString('utf8'))
          iterator.next(append)
        } else {
          iterator.end(() => { resolve(ret) })
        }
      }
      iterator.next(append)
    })
    try {
      let names = await prom
      names.forEach((name) => {
        this.db.del(name, (err) => {
          if (err) {
            console.error('delete node var error', fullname, name, err)
          } else {
            console.log('delete node var ok', fullname, name)
          }
        })
      })
    } catch (e) {
      console.error("children delete error", e)
    }
  }

  async restoreValue(node, fulname, name) {
    return new Promise((resolve, reject) => {
      this.db.get(fulname + '.' + name, (err, value) => {
        if (err) {
          console.error('error loading', fulname + '.' + name, err)
          let cfg = node.variables.get(name)
          if (cfg && cfg.default !== undefined && node.values[name] === undefined) {
            node.set(name, cfg.default)
            //node.values[name] = cfg.default
            if (cfg.forcestore) {
              this.persistValue(fulname + '.' + name, cfg.default, true)
            }
          }
        } else {
          try {
            let truevalue = cbor.decode(value)
            node.set(name, truevalue)
            //node.values[name] = truevalue
            console.log('restore ', fulname + '.' + name, truevalue)
          } catch (cborerr) {
            console.error('cbor err', cborerr)
          }
        }
        resolve()
      })
    });
  }

  async restoreChildren(node, fulname) {
    let keyfrom = fulname + '#'
    let keyto = fulname + '$'
    let prom = new Promise((resolve, reject) => {
      let ret = []
      let param = { gte: keyfrom, lt: keyto }   //param.reverse = true
      let iterator = this.db.iterator(param)
      let append = (err, key, value) => {
        if (err) {
          iterator.end(() => { reject(new Error(err)) })
        } else if (key) {
          let name = key.toString('utf8').replace(/[\.\#]/g, '/')
          let cfg = cbor.decode(value)
          ret.push({ name, cfg })
          iterator.next(append)
        } else {
          iterator.end(() => { resolve(ret) })
        }
      }
      iterator.next(append)
    })
    try {
      let rnodes = await prom
      rnodes.forEach((v) => {
        try {
          let NodeClass
          try {
            NodeClass = require(v.cfg.module)
          } catch (e) {
            NodeClass = require(`${this.localModulesDir}/${v.cfg.module}`)
          }
          let nd = new NodeClass(v.cfg, this)
          node.addNode(v.cfg.name, nd)
        } catch (e) {
          console.error(e)
        }
      })
      let restores = []
      node.nodes.forEach(node => {
        restores.push(this.restoreNode(node, fulname + '.' + node.name))
      })
      return Promise.all(restores)
    } catch (e) {
      console.error("childrenPersistent", e)
    }
  }

  async restoreNode(node, fulname) {
    // restore all variables values from db
    let restores = []
    node.variables.forEach((cfg, name) => {
      if (cfg.persistent) {
        restores.push(this.restoreValue(node, fulname, name))
      }
    })
    await Promise.all(restores)

    if (node.childrenPersistent) {
      return this.restoreChildren(node, fulname)
    } else {
      let restores = []
      //return Promise.all(node.nodes.map(node => this.loadconfig(node, fulname + node.name + '.')))
      node.nodes.forEach(childnode => {
        restores.push(this.restoreNode(childnode, (fulname + '.' + childnode.name).replace(/^\./gm, ''))) //x.replace(/^\s+|\s+$/gm, '');
      })
      return Promise.all(restores)
    }
  }

  dumpdb() {
    //      let keyfrom = fulname + '#'
    //      let keyto = fulname + '$'
    let prom = new Promise((resolve, reject) => {
      let ret = []
      //        let param = { gte: keyfrom, lt: keyto }   //param.reverse = true
      let param = {}
      let iterator = this.db.iterator(param)
      let append = (err, key, value) => {
        if (err) {
          iterator.end(() => { reject(new Error(err)) })
        } else if (key) {
          let name = key.toString('utf8')
          let v = cbor.decode(value)
          console.log('db', JSON.stringify(name), JSON.stringify(v))
          iterator.next(append)
        } else {
          iterator.end(() => { resolve(ret) })
        }
      }
      iterator.next(append)
    })
    return prom
  }

  async connectcloud() {
    try {
      if (this.cfg.servers && Array.isArray(this.cfg.servers)) {
        this.cfg.servers.forEach((srv) => {
          this.UpLink(srv, { realm: this.cfg.type || 'bot', token: this.cfg.token, uid: this.cfg.uid, name: this.cfg.name, login: this.cfg.login, password: this.cfg.password })
        })
      }
    } catch (err) {
      console.error(err)
    }

  }
  async start(cfg) {
    if (cfg === undefined) {
      try {
        this.cfg = JSON.parse(fs.readFileSync('./cfg.json'));
        if (!this.cfg.uid) {
          this.cfg.uid = uidgen.generateSync()
          try {
            fs.writeFileSync('./cfg.json', JSON.stringify(this.cfg, null, ' '), { flag: 'w' })
          } catch (err) {
            console.error(err)
          }
        }
      } catch (err) {
        console.error(err)
        console.error('creating cfg.json file from default')
        let uid = uidgen.generateSync()
        this.cfg = {
          uid: uid,
          type: "bot",
          token: "000000000000000000",
          name: "bot" + uid.substr(0, 4),
          login: "Demo",
          password: "Default",
          servers: [
            "local"
          ]
        }
        fs.writeFileSync('./cfg.json', JSON.stringify(this.cfg, null, ' '), { flag: 'w' })
      }
    } else {
      this.cfg = cfg
    }

    return new Promise((resolve, reject) => {
      this.db = Leveldown(this.cfg.db || './db')

      this.tsdb = new tsdb(this.cfg.tsdb || './tsdb')

      this.db.open(async () => {
        await this.restoreNode(this.node, '')
        this.initialized = true
        try {
          await this.startAllChidren(this.node)
        } catch (e) {
          console.error('start nodes error', e)
        }
        this.connectcloud()
        resolve()
      })
    })
  }
  async startAllChidren(node) {
    let starts = []
    node.nodes.forEach(child_node => {
      if (child_node.start) {
        let p = child_node.start()
        if (p && p.catch)
          starts.push(p.catch(() => { }))
      }
    })
    return Promise.all(starts)
  }

  getValue(uri) { // uri, options, s - storage for late unsubscribe, and emit functions
    if (typeof uri !== 'string') return undefined
    let path = uri.split('/')
    if (path[0] == '') path.shift()

    if (path.length < 1) { return false }
    let node = this.node
    for (let i = 0; i < path.length - 1; i++) {
      if (!node) {
        return false
      } else if (node.getChild) {
        node = node.getChild(path[i])
      } else if (node.get) {
        return node.get(path.splice(i).join('/'))
      }
    }
    if (node && path[path.length - 1] === '*') {
      return node.getAllVars()
    }
    if (node && path[path.length - 1] === '@') {
      return { name: node.name, type: node.get('type') || node.constructor.name, desc: node.get('desc') }
    }
    if (node && node.isChild && node.isChild(path[path.length - 1])) {
      return node.getChild(path[path.length - 1]).getChildredDesc()
    }
    if (node && node.get) {
      return node.get(path[path.length - 1])
    }
    return false
  }
  // setDB(db) {
  //   this.db = db
  // }

  // setHistoryDB(db) {
  //   this.history = db
  // }

  async appendHistory(uri, value, timestamp) {
    this.tsdb.write(uri, value, timestamp)
    // if (this.history)
    //   return this.history.write(uri, value, timestamp)
  }
  async getHistory(uri, from, to, limit) {
    return this.tsdb.select(uri, from, to, limit)
    // if (this.history)
    //   return this.history.select(uri, from, to, limit)
    // return []
  }
  setMeta(uri, metaname, value) { // uri, options, s - storage for late unsubscribe, and emit functions
    if (typeof uri !== 'string') return undefined
    let path = uri.split('/')
    if (path[0] == '') path.shift()

    if (path.length < 1) { return false }
    let node = this.node
    for (let i = 0; i < path.length - 1; i++) {
      if (!node) {
        return false
      } else if (node.getChild) {
        node = node.getChild(path[i])
      } else if (node.setMeta) {
        return node.setMeta(path.splice(i).join('/'), metaname, value)
      }
    }
    if (node && node.setMeta) {
      return node.setMeta(path[path.length - 1], metaname, value)
    }
    return false
  }
  setValue(uri, value) { // uri, options, s - storage for late unsubscribe, and emit functions
    if (typeof uri !== 'string') return undefined
    let path = uri.split('/')
    if (path[0] == '') path.shift()

    if (path.length < 1) { return false }
    let node = this.node
    for (let i = 0; i < path.length - 1; i++) {
      if (!node) {
        return false
      } else if (node.getChild) {
        node = node.getChild(path[i])
      } else if (node.set) {
        return node.set(path.splice(i).join('/'), value)
      }
    }
    if (node && node.set) {
      return node.set(path[path.length - 1], value)
    }
    return false
  }

  updateValue(uri, value) { // update value if property ixists, uri, options, s - storage for late unsubscribe, and emit functions
    if (typeof uri !== 'string') return undefined
    let path = uri.split('/')
    if (path[0] == '') path.shift()

    if (path.length < 1) { return false }
    let node = this.node
    for (let i = 0; i < path.length - 1; i++) {
      if (!node) {
        return false
      } else if (node.getChild) {
        node = node.getChild(path[i])
      } else if (node.has && node.set) {
        let name = path.splice(i).join('/')
        if (node.has(name))
          return node.set(name, value)
        else
          node.onUpdate(name, value)
        return false
      }
    }
    if (node && node.has && node.set) {
      if (node.has(path[path.length - 1]))
        return node.set(path[path.length - 1], value)
      else
        node.onUpdate(path[path.length - 1], value)
    }
    return false
  }

  on(uri, callback) { return this.subscribe(uri, callback) }
  subscribe(uri, callback) { // uri, options, s - storage for late unsubscribe, and emit functions
    if (typeof uri !== 'string') return undefined

    let path = uri.split('/')
    if (path[0] == '') path.shift()

    if (path.length < 1) { return false }
    let node = this.node
    for (let i = 0; i < path.length - 1; i++) {
      if (!node) {
        return false
      } else if (node.getChild) {
        node = node.getChild(path[i])
      } else if (node.subscribe) {
        return node.subscribe(path.splice(i).join('/'), callback)
      }
    }
    if (node && node.subscribe) {
      return node.subscribe(path[path.length - 1], callback)
    }
    return false
  }

  unsubscribe(uri, id) {
    //    const [node, lastname] = this._getNode2(uri)
    let path = uri.split('/')
    if (path[0] == '') path.shift()

    if (path.length < 1) { return false }
    let node = this.node
    for (let i = 0; i < path.length - 1; i++) {
      if (!node) {
        return false
      } else if (node.getChild) {
        node = node.getChild(path[i])
      } else if (node.unsubscribe) {
        return node.unsubscribe(path.splice(i).join('/'), id)
      }
    }
    if (node && node.unsubscribe) {
      let ret = node.unsubscribe(path[path.length - 1], id)
      if (!ret)
        console.error('unsubscribe error', uri, id)
      return ret
    } else {
      console.error('unsubscribe error, uri not found', uri)
    }
    return false
  }

  processIncomeEvent(topic, args, ots) {
    const subskeyParts = topic.split('/')
    let subspath = subskeyParts[0]
    let area = this.root
    //let used = false
    for (let k = 0; k < subskeyParts.length - 1; k++) {
      const t = area.subscriptions['*']
      if (t !== undefined) {
        t.onevent(topic, args, ots)
        //used = true
      }
      area = area.devs && area.devs.get(subskeyParts[k])
      if (!area) break
      subspath = `${subspath}/${subskeyParts[k]}`
    }
    if (area) {
      let fname = `on_${subskeyParts[subskeyParts.length - 1]}`
      const t = area.values[fname]
      if (typeof t === 'function') {
        //used = true
        t(topic, args, ots)
      } else {
        let vname = subskeyParts[subskeyParts.length - 1]
        const t = area.values[vname]
        if (t !== undefined || area.variables.has(vname)) {
          area.set(vname, args)
        } else {
          console.log('unexpected event')
        }
      }
    }
  }

  describe(uri, opts) {
    if (uri === '') {
      return 'js root'
    } else {
      const f = this.urls.get(uri)
      if (f !== undefined) {
        return f.desription
      } else {
        const [link, subaddr, suburi = ''] = this.getLink(uri)
        if (link !== undefined) {
          this.transactionLink(link, subaddr, [6, 0, suburi, args, opts], (answerdata) => {
            this.answer(addr, [7, id, answerdata])
          }, (errcode, errmsg) => {
            this.answer(addr, [5, id, errcode, errmsg])
          })
        } else {
          console.log(`unexpected descr${JSON.stringify(payload)}`)
          this.answer(addr, [5, id, 404, 'no such uri'])
        }
      }
    }
  }

  async call(uri, args) { // call from external device (for example from frontend UI)
    if (uri === '') {
      const ret = {}

      this.node.nodes.forEach((dev, key) => {
        ret[key] = { type: dev.constructor.name }
      })
      //      for (let lnk of this.urls.keys()) {
      //        ret[lnk] = ': function'
      //      }
      // for (let lnk of this.links.keys()) {
      //   ret[lnk] = '*'
      // }
      return Promise.resolve(ret)
    } else {
      // if (this.urls.has(uri)) {
      //   let node = this.urls.get(uri)
      //   if (node.call) return node.call(uri, args)
      //   return Promise.reject({ code: 501, message: 'no call for this node' })
      // }

      let path = uri.split('/')
      if (path[0] == '') path.shift()
      if (path.length < 1) { return Promise.reject({ code: 404, message: '' }) }
      let node = this.node
      for (let i = 0; i < path.length - 1; i++) {
        if (node) {
          if (typeof node.call === 'function') {
            let pathrest = path.slice(i).join('/')
            return node.call(pathrest, args)
          }
          node = node.getChild(path[i])
        }
      }
      if (node) {
        let prop = path[path.length - 1]
        if (node instanceof Node) {
          // let metapos = prop.lastIndexOf('#')
          // if (metapos >= 0) {
          //   let metaname = prop.substring(metapos + 1)
          //   let prop = prop.substring(0,metapos)
          // }

          if (prop.endsWith('#meta')) {
            let metaname = ''
            let vals = node.getMeta(prop.substring(0, prop.length - 5), metaname)
            return vals
          } else if (prop.endsWith('#history')) {
            const { from, to, limit } = args ? args : {}
            return this.getHistory(node.getFullName() + '.' + prop.substring(0, prop.length - 8), from, to, limit)
          } else if (prop.endsWith('@schedule')) {
            if (Array.isArray(args)) node.setSchedule(prop.substring(0, prop.length - 9), args[0])
            return node.getSchedule()
          } else if (node.isFunction(prop)) {
            if (!Array.isArray(args)) args = [args]
            return node.get(prop).apply(node, args)
          } else {
            if (node.has(prop)) {
              if (args !== null && args !== undefined) {
                node.set(prop, args)
              }
              let r = node.get(prop)
              return r
            }
            return Promise.reject({ code: 404, message: 'no such property' })
          }
        } else {
          return Promise.reject({ code: 404, message: 'not a Node' })
        }
      }
      return Promise.reject({ code: 404, message: 'No such uri' })
    }
  }

  UpConnect(url, cfg) {
    let connectcfg = Object.assign({}, { module: 'itmp', name: 'farmbottest', passwd: '222', version: '1.0' }, cfg)
    let lnk = this.addConnector(url, connectcfg)
    lnk.on(lnk.$connected, (data) => {
      console.log('loggedin', data)
    })
    return lnk
  }

  UpLink(srv, cfg) {
    if (srv === 'local') {
      const dgram = require('dgram');
      //const message = Buffer.from('Some bytes');
      const udpclient = dgram.createSocket('udp4');
      udpclient.bind(undefined, undefined, () => {
        udpclient.setBroadcast(true)
      })
      let connected = false
      setInterval(() => {
        if (!connected) {
          udpclient.send(cbor.encode({ bot: 'new bot' }), 38748, '255.255.255.255')
          //udpclient.send(cbor.encode({ bot: 'new bot' }), 38748, 'localhost')
        }
      }, 2000);

      udpclient.on('message', (msg, rinfo) => {
        console.log(`server got: ${msg} from ${rinfo.address}:${rinfo.port}`);
        try {
          connected = true
          let lnk = this.UpConnect(`itmp.ws://${rinfo.address}:38748/robot`, Object.assign({ autoReconnect: false }, cfg))
          lnk.on(lnk.$disconnected, (data) => {
            connected = false
          })
          //let bot = cbor.decode(msg)
          //udpserver.send(cbor.encode({ server: 'new server' }), rinfo.port, rinfo.address)
        } catch {
          ;// hide all errors  
        }
      })
    } else {
      this.UpConnect(`itmp.ws://${srv}:38748/robot`, cfg)
    }
  }
}
//Bot.Node = Node
module.exports = new Core()
