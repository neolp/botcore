const EventEmitter = require('events');
/* eslint-disable no-console */
//const { core } = require('botcorejs')
const vm = require('vm')
const Controlled = require('./controlledarray')

class Node extends EventEmitter {
  constructor(cfg, core) {
    super()
    this.core = core
    this.cfg = cfg
    this.childrenPersistent = typeof cfg === 'object' ? !!cfg.childrenPersistent : false
    this.nodes = new Map() // collection of all subnodes by its name
    this.variables = new Map() // colection of all variables metainformation by variable name
    this.storedtriggers = new Map() // collection  of triggerds by it ID
    this.triggers = {} // collection of triggers by variable names
    this.triggersid = 0 // trigger id serial number
    this.parent = null // link to parent node (null for root)
    this.subscriptions = new Map() // subscriptions by variable names
    this.subscriptionsid = 0 // subscription counter to id generation
    this.values = { console: console, Date: Date, state: 'init' } // direct access from user sandboxed scripts
    //this.addVar('enabled', { default: true, persistent: true })


    const that = this
    this.state = new Proxy(this.values, {
      get(target, prop) {
        if (target[prop] instanceof Node) {
          return target[prop].state
        }
        return target[prop]
      },
      set(target, prop, value) {
        let oldvalue = target[prop]
        target[prop] = value
        that.onUpdate(prop, value, oldvalue)
        return true
      }
    })
    vm.createContext(this.state) // Contextify the sandbox.
  }

  async start() {
    let starts = []
    this.nodes.forEach(node => {
      if (node.start) starts.push(node.start())
    })
    return Promise.all(starts)
  }
  // called then variable was updated (then updated in children prop will be 'child.prop')
  onUpdate(prop, value, oldvalue) {
    if (this.subscriptions.has(prop)) {
      let subs = this.subscriptions.get(prop)
      subs.forEach((sub) => {
        try {
          sub.func(prop, value, oldvalue)
        } catch (er) {
          console.log(er)
        }
      })
    }
    if (this.subscriptions.has('*')) {
      let subs = this.subscriptions.get('*')
      subs.forEach((sub) => {
        try {
          sub.func('*', { [prop]: value }, { [prop]: oldvalue })
        } catch (er) {
          console.log(er)
        }
      })
    }
    // execute trigger for variable named: prop
    let trigger = this.triggers[prop]
    if (trigger) {
      trigger.forEach((tr) => {
        setTimeout(() => {
          try {
            vm.runInContext(tr.funcname + '()', this.state)
          } catch (ex) {
            console.log('ex ' + ex)
          }
        }, 0)
      })
    }
    // try to store persistent vallues to db and to history
    if (this.variables.has(prop)) {
      let h = this.variables.get(prop) // { varname: varname, defval: v, store: store });
      if (h.persistent && value !== oldvalue) {
        let fullname = this.getFullName()
        if (fullname) fullname += '.' + prop
        else fullname = prop
        this.core.persistValue(fullname, value)
      }
      if (h.store) {
        let fullname = this.getFullName('/')
        if (fullname) fullname += '/' + prop
        else fullname = prop
        this.storeAll(h.store, fullname, value)
      }
    }
    if (this.parent) {
      return this.parent.onUpdate(`${this.name}.${prop}`, value, oldvalue)
    }
    return true
  }

  addParent(parent) {
    if (!this.parent) {
      this.parent = parent
    } else {
      console.error('reset parent!!!')
    }
  }

  deleteParent(parent) {
    if (this.parent === parent) {
      this.parent = null
    } else {
      console.error('delete wrong parent!!!')
    }
  }

  subscribe(trigger, func) {
    if (!this.subscriptions.has(trigger)) {
      this.subscriptions.set(trigger, new Map())
    }
    let id = '$' + this.subscriptionsid++
    if (this.subscriptions.get(trigger).size === 0 && this.onsubscribe) {
      this.onsubscribe(trigger)
    }
    this.subscriptions.get(trigger).set(id, { func })
    return id
  }

  unsubscribe(trigger, id) {
    if (!this.subscriptions.has(trigger)) {
      return false
    }
    let ret = this.subscriptions.get(trigger).delete(id)
    if (this.subscriptions.get(trigger).size === 0 && this.onunsubscribe) {
      this.onunsubscribe(trigger)
    }
    return ret
  }

  unsubscribeAll() {
    this.subscriptions.forEach((subs, trigger) => {
      subs.forEach((id, dt) => {

      })
      subs.clear()
      if (this.onunsubscribe)
        this.onunsubscribe(trigger)
    })
    this.subscriptions.clear()
  }

  has(name) {
    return name in this.state
  }
  isVariable(name) {
    return this.variables.has(name)
  }

  set(name, value) {
    this.state[name] = value
    return true
  }

  update(name, value) {
    if (this.state[name] != value){
      this.state[name] = value
      return true
    }
    return false
  }

  get(name) {
    return this.state[name]
  }

  getMeta(name, metaname) {
    let v = this.variables.get(name)
    if (!v) return undefined
    if (!metaname) {
      return Object.assign({}, v)
    }
    return v[metaname]
  }

  setMeta(name, metaname, value) {
    if (!name || !metaname) return
    let v = this.variables.get(name)
    if (!v) this.variables.set(name, { [metaname]: value })
    else v[metaname] = value
  }

  getFull(name) {
    return this.values[name]
  }

  isFunction(name) {
    return typeof this.state[name] === 'function'
  }

  enable() {
    this.set('enabled', true)
  }
  disable() {
    this.set('enabled', false)
  }

  isenabled() {
    return this.get('enabled')
  }

  error(msg) {
    this.set('state', 'error')
    this.set('error', msg)
  }

  populate(fnames) {
    fnames.forEach(element => {
      if (typeof this[element] === 'function')
        this.values[element] = this[element].bind(this)
      else
        this.values[element] = this[element]
    })
  }
  getFullName(infix = '.') {
    let prefix = ''
    if (this.parent && this.parent.getFullName) prefix = this.parent.getFullName()
    if (prefix) return `${prefix}${infix}${this.name}`
    return this.name || ''
  }

  store(stor, name, value) {
    let storagename
    if (typeof stor === 'boolean') {
      storagename = 'tsdb'
    } else {
      storagename = stor.storage || 'tsdb'
    }
    if (stor.throttle) {
      if (stor.throttle.type === 'skip') {
        let timestamp = Date.now()
        if (stor.timestamp && timestamp - stor.timestamp < stor.throttle.period) { return }
        stor.timestamp = timestamp
      }
    }
    this.core.appendHistory(name, value)
  }

  storeAll(stor, name, value) {
    if (Array.isArray(stor)) {
      stor.map((val) => { this.store(val, name, value) })
    } else {
      this.store(stor, name, value)
    }

  }


  // add new trigger with appropriate name, trigger vriables, function source code, some text and language
  addTrigger(triggername, triggers, funcstart, funcstop, language) {
    if (triggername && this.storedtriggers.has(triggername)) {
      this.deleteTrigger(triggername)
    }
    // let fname = 'f' + this.cnt++
    if (!Array.isArray(triggers)) {
      triggers = triggers.split(',')
    }
    if (funcstart instanceof Function) {
      funcstart = funcstart.toString()
    }
    if (typeof funcstart !== 'string' && !(funcstart instanceof String)) {
      return
    }
    try {
      let id = '$' + this.triggersid++
      vm.runInContext(id + ' = async () => { ' + funcstart + ' }', this.state)
      if (funcstop)
        vm.runInContext(id + 'stop = async () => { ' + funcstop + ' }', this.state)
      triggers.forEach((currentValue) => {
        if (!this.triggers[currentValue]) {
          this.triggers[currentValue] = new Set() // [triggername]
          let startcontrol
          if (timecontrollers[currentValue] || ((startcontrol = currentValue.indexOf(' ')) >= 0 && timecontrollers[currentValue.substr(0, startcontrol)])) {
            let tcontroller = timecontrollers[currentValue.substr(0, startcontrol)]
            tcontroller.start(this, currentValue)
          }
        }
        let tr = { func: id }
        if (funcstop) tr.funcstop = id + 'stop'
        this.triggers[currentValue].add(tr)
      })
    } catch (ex) {
      console.log(triggername + ' exc ' + ex)
    }

    // let triggersname = triggers.join()
    this.storedtriggers.set(triggername, { triggername: triggername, triggers: triggers, start: funcstart, stop: funcstop, language: language })
    this.savecfg()
  }

  // delete trigger by name
  deleteTrigger(triggername) {
    try {
      delete this.state[triggername] // remove function from context
      let triggervars = this.storedtriggers.get(triggername) // remove trigger variables from list
      for (let i = 0; i < triggervars.triggers.length; i++) {
        let tv = triggervars.triggers[i]
        this.triggers[tv].delete(triggername)
        let startcontrol
        if (timecontrollers[tv] || ((startcontrol = tv.indexOf(' ')) >= 0 && timecontrollers[tv.substr(0, startcontrol)])) {
          let tcontroller = timecontrollers[tv.substr(0, startcontrol)]
          if (this.triggers[tv].size === 0) {
            tcontroller.stop(this, tv)
          }
        }
        if (this.triggers[tv].size === 0) {
          delete this.triggers[tv]
        }
      }
    } catch (ex) {
      console.log(triggername + ' exc ' + ex)
    }
    this.storedtriggers.delete(triggername)
    this.savecfg()
  }

  isChild(name) {
    return this.nodes.has(name)
  }

  getChild(name) {
    return this.nodes.get(name)
  }

  getChildredDesc() {
    let ret = []
    for (let node of this.nodes.keys()) {
      ret.push({ name: node, type: this.nodes.get(node).get('type') || this.nodes.get(node).constructor.name, desc: this.nodes.get(node).get('desc') })
    }
    for (let variable of this.variables.keys()) {
      ret.push({ name: variable, type: 'Variable', desc: this.variables.get(variable) })
    }

    return ret
  }
  getAllVars() {
    let ret = {}
    for (let variable of this.variables.keys()) {
      ret[variable] = this.get(variable)
    }
    return ret
  }

  getNode(uri) {
    let path = uri.split('/')
    if (path[0] === '') path.shift()
    let it = this.getChild(path[0])
    for (let i = 1; i < path.length; i++) {
      if (it) {
        it = it.getChild(path[i])
      }
    }
    return it
  }
  getDevices() {
    return Promise.resolve(this.nodes)
  }

  savecfg() {
    if (this.model) {
      this.model.savecfg()
    }
  }

  getSchedule(name) {
    let dev = this.getChild(name)
    if (dev) {
      return dev.cfg.schedule
    }
  }

  setSchedule(name, schedule) {
    let dev = this.getChild(name)
    if (dev) {
      dev.cfg.schedule = schedule
      if (dev.scheduler) dev.scheduler.stop()
      if (dev.cfg.schedule) {
        let t = new LpInterval(dev.cfg.schedule, () => {
          let on = dev.get('on')
          if (on) { on() }
        }, () => {
          let off = dev.get('off')
          if (off) { off() }
        })
        dev.scheduler = t
        t.start()
      }
    }
  }

  addNode(nodeName, node) {
    let oldvalue = this.getChildredDesc()
    node.addParent(this)
    node.name = nodeName // ???
    this.nodes.set(nodeName, node)
    this.values[nodeName] = node
    if (this.parent && this.parent.onUpdate)
      this.parent.onUpdate(`${this.name}`, this.getChildredDesc(), oldvalue)
    if (this.childrenPersistent) {
      this.core.persistChild(this.getFullName(), nodeName, node)
    }
    //this.setSchedule(name, cfg.schedule)
    //this.savecfg()
  }
  deleteNode(nodeName) {
    if (this.nodes.has(nodeName)) {
      let oldvalue = this.getChildredDesc()
      this.nodes.delete(nodeName)
      delete this.values[nodeName]
      this.unsubscribeAll()
      if (this.parent && this.parent.onUpdate)
        this.parent.onUpdate(`${this.name}`, this.getChildredDesc(), oldvalue)
      if (this.childrenPersistent) {
        this.core.persistChildDelete(this.getFullName(), nodeName)
      }
    }
    //this.savecfg()
    return true
  }

  addVar(varname, cfg) {// {default:<value>, store:<store cfg>, ) {
    let config = Object.assign({}, this.variables.get(varname), cfg) // create new configuration object
    config.varname = varname
    if (!config.store) config.store = false
    this.variables.set(varname, config)
    this.values[varname] = cfg.default

    // let that = this
    // if (cfg.source) {
    //   if (this.variables.get(varname).subscribtion) {
    //     this.model.unsubscribeDevice(cfg.source, this.variables.get(varname).subscription)
    //   }
    //   this.variables.get(varname).subscription = this.model.subscribeDevice(cfg.source, (name, value) => {
    //     that.set(varname, value)
    //   })
    // }
    // this.savecfg()
  }

  serialize() {
    let cfg = {}
    let nodes = []
    let areas = []

    this.nodes.forEach((value, name) => {
      let d
      if (value instanceof Area) {
        d = value.serialize()
        d.name = name
        d.type = 'Area'
        areas.push(d)
        //d = {name: value.name, area: value.serialize()}
      } else {
        d = { name: name, type: value.type, connector: value.connector, key: value.addr }
        nodes.push(d)
      }
    })
    if (nodes)
      cfg.nodes = nodes
    if (areas)
      cfg.areas = areas

    let cfg_vars = []
    this.variables.forEach((value, vname) => {
      let vstore = value.cfg
      vstore.varname = vname
      vstore.value = this.values[vname]
      cfg_vars.push(vstore) // { varname: vname, defval: value.defval, store: value.store })
    })
    if (cfg_vars.length > 0) {
      cfg.vars = cfg_vars
    }
    let cfg_triggers = []
    this.storedtriggers.forEach((value, key) => {
      cfg_triggers.push({ triggername: key, triggers: value.triggers, source: value.source, text: value.text, language: value.language })
    })
    if (cfg_triggers.length > 0) {
      cfg.triggers = cfg_triggers
    }
    return cfg
  }



  deserialize(cfg, areasController, path = '') {
    try {
      if (cfg.nodes) {
        for (let dev in cfg.nodes) {
          try {
            if (cfg.nodes[dev].devtype)
              cfg.nodes[dev].type = cfg.nodes[dev].devtype
            this.addDev(cfg.nodes[dev].device, cfg.nodes[dev].name, cfg.nodes[dev]) // //    this.tokens.set('35234512423', {ok: 'kok', link: '/office/dv'})         this.tokens.set('24398562394875', {ok: 'kok', link: '/office/dv'})          this.tokens.set('qwertyuioplkjhgf', {ok: 'kok', link: '/office/farm'})
          } catch (err) {
            console.error(err)
          }
        }
      }
      if (cfg.areas) {
        for (let area in cfg.areas) {
          try {
            if (cfg.areas[area].source) {
              let a = new AreaProxy(this.model, cfg.areas[area].name, cfg.areas[area])
              this.addArea(a)
            } else {
              let a = new Area(this.model, cfg.areas[area].name, cfg.areas[area])
              a.deserialize(cfg.areas[area], areasController, path + '/' + cfg.areas[area].name)
              this.addArea(a)
              if (cfg.areas[area].token) {
                areasController.addToken(cfg.areas[area].token, path + '/' + cfg.areas[area].name)
              }
            }
          } catch (err) {
            console.error(err)
          }
        }
      }
      if (cfg.vars) {
        for (let hist in cfg.vars) {
          this.addVar(cfg.vars[hist].varname, cfg.vars[hist])
        }
      }
      if (cfg.triggers) {
        for (let trig in cfg.triggers) {
          if (cfg.triggers[trig].file) {
            cfg.triggers[trig].start = fs.readFileSync(cfg.triggers[trig].file, 'utf8')
          }
          if (cfg.triggers[trig].source && !cfg.triggers[trig].start) {
            cfg.triggers[trig].start = cfg.triggers[trig].source
          }
          this.addTrigger(cfg.triggers[trig].triggername, cfg.triggers[trig].triggers, cfg.triggers[trig].start, cfg.triggers[trig].stop, cfg.triggers[trig].language)
        }
      }
    } catch (er) {
      console.error(er)
    }
  }
}

module.exports = Node