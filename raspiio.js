const Node = require('./botnode')
const rpio = require('rpio')

class RaspiIO extends Node {

  constructor(core, cfg) {
    super(core, cfg)
    var { connector, fullhost } = core.getConnector(cfg.addr)
    this.connector = connector
    this.addr = fullhost
    //    [this.connector, this.addr] = core.getConnector(cfg.addr)

    var options = {
      gpiomem: false,          /* Use /dev/gpiomem */
      mapping: 'physical',//'gpio',//'physical',    /* Use the P1-P40 numbering scheme */
      mock: undefined,        /* Emulate specific hardware in mock mode */
    }
    rpio.init([options])
    rpio.open(26, rpio.OUTPUT, rpio.LOW)
    // rpio.write(26, rpio.HIGH)
  }

  call(addr, topic, arg) {
    switch (topic) {
    case 'set':
      rpio.write(addr, arg ? rpio.HIGH : rpio.LOW)
      break
    case 'config':
      rpio.open(addr, rpio.OUTPUT, rpio.LOW)
      break
    }

  }
  onconnect(num) {
    if (num == 0) {
      rpio.write(26, rpio.HIGH)
    }
  }
  ondisconnect(num) {
    if (num == 0) {
      rpio.write(26, rpio.LOW)
    }
  }
}

module.exports = RaspiIO